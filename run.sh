# Uruchomienie programu
java -jar green-energy/target/green-energy-1.0.0.jar

# Sprawdzenie, czy uruchomienie się powiodło
if [ $? -ne 0 ]; then
    echo "Błąd podczas uruchamiania programu."
    exit 1
fi

echo "Program zakończył działanie."