package com.rudnick.competition.greenenergy.onlinegame.controller;

import com.rudnick.competition.greenenergy.onlinegame.model.Players;
import com.rudnick.competition.greenenergy.onlinegame.service.OnlineGameApiService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class OnlineGameApiControllerTest {

    @Mock
    private OnlineGameApiService service;

    @InjectMocks
    private OnlineGameApiController controller;

    @Test
    void should_call_calculate_when_request() {
        //given
        final Players players = mock(Players.class);

        //when
        final ResponseEntity response = controller.calculate(players);

        //then
        assertTrue(response.getStatusCode().is2xxSuccessful());
        verify(service).calculate(players);
    }

}