package com.rudnick.competition.greenenergy.onlinegame.service;

import com.rudnick.competition.greenenergy.onlinegame.model.Clan;
import com.rudnick.competition.greenenergy.onlinegame.model.Players;

import java.util.List;

import static java.util.Collections.singletonList;

final class Fixture {

    static final int GROUP_COUNT_5 = 5;
    static final int POINT_15 = 15;
    static final int NUMBER_OF_PLAYERS_4 = 4;
    static final int POINTS_5 = 5;
    static final int NUMBER_OF_PLAYERS_2 = 2;
    private static final int GROUP_COUNT_4 = 4;
    private static final int NUMBER_OF_PLAYERS_3 = 3;
    private static final int POINTS_10 = 10;

    static Clan getClan() {
        return getClanBuilder().build();
    }

    static Clan getClan(final int point, final int numberOfPlayers) {
        return getClanBuilder()
                .points(point)
                .numberOfPlayers(numberOfPlayers)
                .build();
    }

    static Players getPlayers() {
        return getPlayersBuilder().build();
    }

    static Players getPlayers(final int groupCount, final List<Clan> clams) {
        return getPlayersBuilder()
                .groupCount(groupCount)
                .clans(clams)
                .build();
    }

    private static Players.PlayersBuilder getPlayersBuilder() {
        return Players.builder()
                .groupCount(GROUP_COUNT_4)
                .clans(singletonList(getClanBuilder().build()));
    }

    private static Clan.ClanBuilder getClanBuilder() {
        return Clan.builder()
                .numberOfPlayers(NUMBER_OF_PLAYERS_3)
                .points(POINTS_10);
    }

    private Fixture() {
    }
}
