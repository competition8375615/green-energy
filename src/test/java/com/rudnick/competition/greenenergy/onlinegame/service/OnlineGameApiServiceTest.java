package com.rudnick.competition.greenenergy.onlinegame.service;

import com.rudnick.competition.greenenergy.onlinegame.model.Clan;
import com.rudnick.competition.greenenergy.onlinegame.model.Players;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static com.rudnick.competition.greenenergy.onlinegame.service.Fixture.*;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class OnlineGameApiServiceTest {

    private OnlineGameApiService service = new OnlineGameApiService();

    @Test
    void should_return_npe_when_null_parameter() {
        //when then
        assertThrows(NullPointerException.class, () -> service.calculate(null));
    }

    @ParameterizedTest
    @MethodSource("provideDataForCalculateServiceTest")
    void should_return_atm_list_when_calculate_tasks(final Players players, final List<List<Clan>> expected) {
        //when
        final List<List<Clan>> actual = service.calculate(players);

        //then
        assertEquals(actual.size(), expected.size());
        assertEquals(actual, expected);
    }

    private static Stream<Arguments> provideDataForCalculateServiceTest() {
        return Stream.of(
                Arguments.of(getPlayers(), singletonList(singletonList(getClan()))),
                Arguments.of(getPlayers(GROUP_COUNT_5, Arrays.asList(getClan(), getClan(POINTS_5, NUMBER_OF_PLAYERS_2), getClan(POINT_15, NUMBER_OF_PLAYERS_4))),
                        Arrays.asList(singletonList(getClan(POINT_15, NUMBER_OF_PLAYERS_4)), Arrays.asList(getClan(), getClan(POINTS_5, NUMBER_OF_PLAYERS_2))))
        );
    }
}