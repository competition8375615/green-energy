package com.rudnick.competition.greenenergy.onlinegame.controller;

final class Fixture {
    static final String PETS_PATH = "/onlinegame/calculate";

    static final String EXAMPLE_REQUEST_CONTENT = "{" +
            "  \"groupCount\": 6," +
            "  \"clans\": [" +
            "    {" +
            "      \"numberOfPlayers\": 4," +
            "      \"points\": 50" +
            "    }," +
            "    {" +
            "      \"numberOfPlayers\": 2," +
            "      \"points\": 70" +
            "    }," +
            "    {" +
            "      \"numberOfPlayers\": 6," +
            "      \"points\": 60" +
            "    }," +
            "    {" +
            "      \"numberOfPlayers\": 1," +
            "      \"points\": 15" +
            "    }," +
            "    {" +
            "      \"numberOfPlayers\": 1," +
            "      \"points\": 5" +
            "    }," +
            "    {" +
            "      \"numberOfPlayers\": 1," +
            "      \"points\": 26" +
            "    }," +
            "    {" +
            "      \"numberOfPlayers\": 5," +
            "      \"points\": 40" +
            "    }," +
            "    {" +
            "      \"numberOfPlayers\": 3," +
            "      \"points\": 45" +
            "    }," +
            "    {" +
            "      \"numberOfPlayers\": 1," +
            "      \"points\": 12" +
            "    }," +
            "    {" +
            "      \"numberOfPlayers\": 4," +
            "      \"points\": 40" +
            "    }" +
            "  ]" +
            "}";

    static final String EXPECTED_RESPONSE = "[" +
            "[" +
                "{\"numberOfPlayers\":2,\"points\":70}," +
                "{\"numberOfPlayers\":4,\"points\":50}]," +
            "[" +
                "{\"numberOfPlayers\":6,\"points\":60}]," +
            "[" +
                "{\"numberOfPlayers\":3,\"points\":45}," +
                "{\"numberOfPlayers\":1,\"points\":26}," +
                "{\"numberOfPlayers\":1,\"points\":15}," +
                "{\"numberOfPlayers\":1,\"points\":12}]," +
            "[" +
                "{\"numberOfPlayers\":5,\"points\":40}," +
                "{\"numberOfPlayers\":1,\"points\":5}]," +
            "[" +
                "{\"numberOfPlayers\":4,\"points\":40}]" +
            "]";

    private Fixture() {
    }
}
