package com.rudnick.competition.greenenergy.atmservice.service;

import com.rudnick.competition.greenenergy.atmservice.model.ATM;
import com.rudnick.competition.greenenergy.atmservice.model.RequestTypeEnum;
import com.rudnick.competition.greenenergy.atmservice.model.Task;

import static com.rudnick.competition.greenenergy.atmservice.model.RequestTypeEnum.STANDARD;

final class Fixture {

    static final int ATM_ID_2 = 2;
    static final int ATM_ID_3 = 3;
    static final int ATM_ID_4 = 4;
    static final int DEFAULT_REGION = 1;
    static final int REGION_2 = 2;

    private static final int DEFAULT_ATM_ID = 1;
    private static final RequestTypeEnum DEFAULT_TASK_REQUEST_TYPE = STANDARD;

    static Task getTask() {
        return getTaskBuilder().build();
    }

    static Task getTask(final RequestTypeEnum requestType, final int atmId) {
        return getTaskBuilder()
                .atmId(atmId)
                .requestType(requestType)
                .build();
    }

    static Task getTask(final RequestTypeEnum requestType, final int atmId, final int region) {
        return getTaskBuilder()
                .atmId(atmId)
                .requestType(requestType)
                .region(region)
                .build();
    }

    static ATM getATM() {
        return getATMBuilder().build();
    }

    static ATM getATM(final int atmId, final int region) {
        return getATMBuilder()
                .atmId(atmId)
                .region(region)
                .build();
    }

    private static Task.TaskBuilder getTaskBuilder() {
        return Task.builder()
                .atmId(DEFAULT_ATM_ID)
                .requestType(DEFAULT_TASK_REQUEST_TYPE)
                .region(DEFAULT_REGION);
    }

    private static ATM.ATMBuilder getATMBuilder() {
        return ATM.builder()
                .atmId(DEFAULT_ATM_ID)
                .region(DEFAULT_REGION);
    }

    private Fixture() {
    }
}
