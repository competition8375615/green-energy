package com.rudnick.competition.greenenergy.atmservice.service;

import com.rudnick.competition.greenenergy.atmservice.model.ATM;
import com.rudnick.competition.greenenergy.atmservice.model.Task;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.stream.Stream;

import static com.rudnick.competition.greenenergy.atmservice.model.RequestTypeEnum.*;
import static com.rudnick.competition.greenenergy.atmservice.service.Fixture.*;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AtmsApiServiceTest {

    @Mock
    private TaskToATMMapper taskToATMMapper;

    @InjectMocks
    private AtmsApiService service;

    @Test
    void should_return_npe_when_null_parameter() {
        //when then
        assertThrows(NullPointerException.class, () -> service.calculate(null));
    }


    @ParameterizedTest
    @MethodSource("provideDataForCalculateServiceTest")
    void should_return_atm_list_when_calculate_tasks(final List<Task> tasks, final List<ATM> expected) {
        //given
        tasks.forEach(task ->
                when(taskToATMMapper.map(task)).thenReturn(getAtmForTask(expected, task))
        );

        //when
        final List<ATM> actual = service.calculate(tasks);

        //then
        assertEquals(expected.size(), actual.size());
        assertEquals(expected, actual);
    }

    private ATM getAtmForTask(List<ATM> atms, Task task) {
        return atms.stream()
                .filter(atm -> atm.getAtmId()
                        .equals(task.getAtmId())).findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    private static Stream<Arguments> provideDataForCalculateServiceTest() {
        return Stream.of(
                Arguments.of(singletonList(getTask()), singletonList(getATM())),
                Arguments.of(asList(getTask(), getTask(PRIORITY, ATM_ID_2)),
                        asList(getATM(ATM_ID_2, DEFAULT_REGION), getATM())),
                Arguments.of(asList(getTask(), getTask(PRIORITY, ATM_ID_2), getTask(FAILURE_RESTART, ATM_ID_3), getTask(SIGNAL_LOW, ATM_ID_4)),
                        asList(getATM(ATM_ID_3, DEFAULT_REGION), getATM(ATM_ID_2, DEFAULT_REGION), getATM(), getATM(ATM_ID_4, DEFAULT_REGION))),
                Arguments.of(asList(getTask(), getTask(PRIORITY, ATM_ID_2, REGION_2), getTask(FAILURE_RESTART, ATM_ID_3, REGION_2), getTask(SIGNAL_LOW, ATM_ID_4, REGION_2)),
                        asList(getATM(), getATM(ATM_ID_3, REGION_2), getATM(ATM_ID_2, REGION_2), getATM(ATM_ID_4, REGION_2)))
        );
    }
}