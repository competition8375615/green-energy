package com.rudnick.competition.greenenergy.atmservice.controller;


import com.rudnick.competition.greenenergy.atmservice.service.AtmsApiService;
import com.rudnick.competition.greenenergy.atmservice.service.TaskToATMMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static com.rudnick.competition.greenenergy.atmservice.controller.Fixture.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest({AtmsApiController.class, AtmsApiService.class, TaskToATMMapper.class})
class AtmsApiControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_example_response_when_sent_example_request() throws Exception {
        // When
        final MvcResult response = this.mockMvc.perform(
                post(PETS_PATH)
                        .contentType(APPLICATION_JSON)
                        .content(EXAMPLE_REQUEST_CONTENT))
                .andExpect(status().isOk())
                .andReturn();
        // Then
        assertThat(response.getResponse().getContentAsString())
                .isEqualTo(EXPECTED_RESPONSE);
    }
}