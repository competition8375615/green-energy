package com.rudnick.competition.greenenergy.atmservice.controller;

import com.rudnick.competition.greenenergy.atmservice.model.Task;
import com.rudnick.competition.greenenergy.atmservice.service.AtmsApiService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class AtmsApiControllerTest {

    @Mock
    private AtmsApiService service;

    @InjectMocks
    private AtmsApiController controller;

    @Test
    void should_call_calculate_when_request() {
        //given
        final Task task = mock(Task.class);

        //when
        final ResponseEntity response = controller.calculate(singletonList(task));

        //then
        assertTrue(response.getStatusCode().is2xxSuccessful());
        verify(service).calculate(singletonList(task));
    }

}