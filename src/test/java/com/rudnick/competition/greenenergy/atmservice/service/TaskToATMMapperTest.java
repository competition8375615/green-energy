package com.rudnick.competition.greenenergy.atmservice.service;

import com.rudnick.competition.greenenergy.atmservice.model.ATM;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TaskToATMMapperTest {
    private final TaskToATMMapper mapper = new TaskToATMMapper();

    @Test
    void should_return_npe_when_null_parameter() {
        //when then
        assertThrows(NullPointerException.class, () -> mapper.map(null));
    }

    @Test
    void should_map_task() {
        // When
        final ATM actual = mapper.map(Fixture.getTask());
        // Then
        assertEquals(Fixture.getATM(), actual);
    }
}