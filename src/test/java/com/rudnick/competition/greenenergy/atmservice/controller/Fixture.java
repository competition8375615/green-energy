package com.rudnick.competition.greenenergy.atmservice.controller;

import com.rudnick.competition.greenenergy.atmservice.model.ATM;
import com.rudnick.competition.greenenergy.atmservice.model.RequestTypeEnum;
import com.rudnick.competition.greenenergy.atmservice.model.Task;

import static com.rudnick.competition.greenenergy.atmservice.model.RequestTypeEnum.STANDARD;

final class Fixture {
    static final String PETS_PATH = "/atms/calculateOrder";

    private static final int DEFAULT_REGION = 1;
    private static final int DEFAULT_ATM_ID = 1;
    private static final RequestTypeEnum DEFAULT_TASK_REQUEST_TYPE = STANDARD;

    static final String EXAMPLE_REQUEST_CONTENT = "[" +
            "  {" +
            "    \"region\": 4," +
            "    \"requestType\": \"STANDARD\"," +
            "    \"atmId\": 1" +
            "  }," +
            "  {" +
            "    \"region\": 1," +
            "    \"requestType\": \"STANDARD\"," +
            "    \"atmId\": 1" +
            "  }," +
            "  {" +
            "    \"region\": 2," +
            "    \"requestType\": \"STANDARD\"," +
            "    \"atmId\": 1" +
            "  }," +
            "  {" +
            "    \"region\": 3," +
            "    \"requestType\": \"PRIORITY\"," +
            "    \"atmId\": 2" +
            "  }," +
            "  {" +
            "    \"region\": 3," +
            "    \"requestType\": \"STANDARD\"," +
            "    \"atmId\": 1" +
            "  }," +
            "  {" +
            "    \"region\": 2," +
            "    \"requestType\": \"SIGNAL_LOW\"," +
            "    \"atmId\": 1" +
            "  }," +
            "  {" +
            "    \"region\": 5," +
            "    \"requestType\": \"STANDARD\"," +
            "    \"atmId\": 2" +
            "  }," +
            "  {" +
            "    \"region\": 5," +
            "    \"requestType\": \"FAILURE_RESTART\"," +
            "    \"atmId\": 1" +
            "  }" +
            "]";

    static final String EXPECTED_RESPONSE = "[" +
            "{\"region\":1,\"atmId\":1}," +
            "{\"region\":2,\"atmId\":1}," +
            "{\"region\":3,\"atmId\":2}," +
            "{\"region\":3,\"atmId\":1}," +
            "{\"region\":4,\"atmId\":1}," +
            "{\"region\":5,\"atmId\":1}," +
            "{\"region\":5,\"atmId\":2}" +
            "]";


    static Task getTask() {
        return getTaskBuilder().build();
    }

    static ATM getATM() {
        return getATMBuilder().build();
    }

    private static Task.TaskBuilder getTaskBuilder() {
        return Task.builder()
                .atmId(DEFAULT_ATM_ID)
                .requestType(DEFAULT_TASK_REQUEST_TYPE)
                .region(DEFAULT_REGION);
    }

    private static ATM.ATMBuilder getATMBuilder() {
        return ATM.builder()
                .atmId(DEFAULT_ATM_ID)
                .region(DEFAULT_REGION);
    }

    private Fixture() {
    }
}
