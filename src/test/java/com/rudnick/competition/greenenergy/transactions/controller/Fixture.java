package com.rudnick.competition.greenenergy.transactions.controller;

final class Fixture {
    static final String PETS_PATH = "/transactions/report";

    static final String EXAMPLE_REQUEST_CONTENT = "[" +
            "  {" +
            "    \"debitAccount\": \"32309111922661937852684864\"," +
            "    \"creditAccount\": \"06105023389842834748547303\"," +
            "    \"amount\": 10.90" +
            "  }," +
            "  {" +
            "    \"debitAccount\": \"31074318698137062235845814\"," +
            "    \"creditAccount\": \"66105036543749403346524547\"," +
            "    \"amount\": 200.90" +
            "  }," +
            "  {" +
            "    \"debitAccount\": \"66105036543749403346524547\"," +
            "    \"creditAccount\": \"32309111922661937852684864\"," +
            "    \"amount\": 50.10" +
            "  }" +
            "]";

    static final String EXPECTED_RESPONSE = "[" +
            "{\"account\":\"06105023389842834748547303\",\"debitCount\":0,\"creditCount\":1,\"balance\":10.90}," +
            "{\"account\":\"31074318698137062235845814\",\"debitCount\":1,\"creditCount\":0,\"balance\":-200.90}," +
            "{\"account\":\"32309111922661937852684864\",\"debitCount\":1,\"creditCount\":1,\"balance\":39.20}," +
            "{\"account\":\"66105036543749403346524547\",\"debitCount\":1,\"creditCount\":1,\"balance\":150.80}" +
            "]";

    private Fixture() {
    }
}
