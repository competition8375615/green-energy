package com.rudnick.competition.greenenergy.transactions.service;

import com.rudnick.competition.greenenergy.transactions.model.Account;
import com.rudnick.competition.greenenergy.transactions.model.Transaction;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static com.rudnick.competition.greenenergy.transactions.service.Fixture.*;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TransactionsApiServiceTest {

    private TransactionsApiService service = new TransactionsApiService();

    @Test
    void should_return_npe_when_null_parameter() {
        //when then
        assertThrows(NullPointerException.class, () -> service.report(null));
    }

    @ParameterizedTest
    @MethodSource("provideDataForCalculateServiceTest")
    void should_return_atm_list_when_calculate_tasks(final List<Transaction> transactions, final List<Account> expected) {
        //when
        final List<Account> actual = service.report(transactions);

        //then
        assertEquals(actual.size(), expected.size());
        assertEquals(actual, expected);
    }

    private static Stream<Arguments> provideDataForCalculateServiceTest() {
        return Stream.of(
                Arguments.of(singletonList(getTransaction()), asList(getFirstAccount(), getSecondAccount())),
                Arguments.of(asList(getTransaction(), getSecondTransaction()),
                        asList(getFirstAccount(), getSecondAccountAfterTwoTransactions(), getThirdAccount()))
        );
    }
}