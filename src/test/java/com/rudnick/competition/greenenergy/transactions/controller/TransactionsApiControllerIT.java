package com.rudnick.competition.greenenergy.transactions.controller;

import com.rudnick.competition.greenenergy.transactions.service.TransactionsApiService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static com.rudnick.competition.greenenergy.transactions.controller.Fixture.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest({TransactionsApiController.class, TransactionsApiService.class})
class TransactionsApiControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_example_response_when_sent_example_request() throws Exception {
        // When
        final MvcResult response = this.mockMvc.perform(
                post(PETS_PATH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(EXAMPLE_REQUEST_CONTENT))
                .andExpect(status().isOk())
                .andReturn();
        // Then
        assertThat(response.getResponse().getContentAsString()).isEqualTo(EXPECTED_RESPONSE);
    }
}