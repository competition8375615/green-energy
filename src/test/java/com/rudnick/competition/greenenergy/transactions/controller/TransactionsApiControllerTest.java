package com.rudnick.competition.greenenergy.transactions.controller;

import com.rudnick.competition.greenenergy.transactions.model.Transaction;
import com.rudnick.competition.greenenergy.transactions.service.TransactionsApiService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class TransactionsApiControllerTest {

    @Mock
    private TransactionsApiService service;

    @InjectMocks
    private TransactionsApiController controller;

    @Test
    void should_call_report_when_request() {
        //given
        final Transaction transaction = mock(Transaction.class);

        //when
        final ResponseEntity response = controller.report(singletonList(transaction));

        //then
        assertTrue(response.getStatusCode().is2xxSuccessful());
        verify(service).report(singletonList(transaction));
    }

}