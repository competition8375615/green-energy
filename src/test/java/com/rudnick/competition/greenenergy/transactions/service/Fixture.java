package com.rudnick.competition.greenenergy.transactions.service;

import com.rudnick.competition.greenenergy.transactions.model.Account;
import com.rudnick.competition.greenenergy.transactions.model.Transaction;

import java.math.BigDecimal;

final class Fixture {

    private static final String ACCOUNT_NUMBER_1 = "ACC_1";
    private static final String ACCOUNT_NUMBER_2 = "ACC_2";
    private static final String ACCOUNT_NUMBER_3 = "ACC_3";

    private static final BigDecimal ACCOUNT_1_BALANCE = BigDecimal.valueOf(100);
    private static final BigDecimal ACCOUNT_2_BALANCE = BigDecimal.valueOf(-100);
    private static final BigDecimal ACCOUNT_2_BALANCE_TWO_TRANS= BigDecimal.valueOf(-150);
    private static final BigDecimal ACCOUNT_3_BALANCE = BigDecimal.valueOf(50);

    private static final int CREDIT_COUNT_ACCOUNT_1 = 1;
    private static final int DEBIT_COUNT_ACCOUNT_1 = 0;

    private static final int CREDIT_COUNT_ACCOUNT_2 = 0;
    private static final int DEBIT_COUNT_ACCOUNT_2 = 1;

    private static final int CREDIT_COUNT_ACCOUNT_2_TWO_TRANS = 0;
    private static final int DEBIT_COUNT_ACCOUNT_2_TWO_TRANS = 2;

    private static final int CREDIT_COUNT_ACCOUNT_3 = 1;
    private static final int DEBIT_COUNT_ACCOUNT_3 = 0;

    private static final BigDecimal TRANSACTION_1_AMOUNT = BigDecimal.valueOf(100);
    private static final BigDecimal TRANSACTION_2_AMOUNT = BigDecimal.valueOf(50);

    static Account getFirstAccount() {
        return getAccountBuilder().build();
    }

    static Account getSecondAccount() {
        return getAccountBuilder()
                .account(ACCOUNT_NUMBER_2)
                .balance(ACCOUNT_2_BALANCE)
                .creditCount(CREDIT_COUNT_ACCOUNT_2)
                .debitCount(DEBIT_COUNT_ACCOUNT_2)
                .build();
    }

    static Account getSecondAccountAfterTwoTransactions() {
        return getAccountBuilder()
                .account(ACCOUNT_NUMBER_2)
                .balance(ACCOUNT_2_BALANCE_TWO_TRANS)
                .creditCount(CREDIT_COUNT_ACCOUNT_2_TWO_TRANS)
                .debitCount(DEBIT_COUNT_ACCOUNT_2_TWO_TRANS)
                .build();
    }

    static Account getThirdAccount() {
        return getAccountBuilder()
                .account(ACCOUNT_NUMBER_3)
                .balance(ACCOUNT_3_BALANCE)
                .creditCount(CREDIT_COUNT_ACCOUNT_3)
                .debitCount(DEBIT_COUNT_ACCOUNT_3)
                .build();
    }

    private static Account.AccountBuilder getAccountBuilder() {
        return Account.builder()
                .balance(ACCOUNT_1_BALANCE)
                .account(ACCOUNT_NUMBER_1)
                .creditCount(CREDIT_COUNT_ACCOUNT_1)
                .debitCount(DEBIT_COUNT_ACCOUNT_1);
    }

    static Transaction getTransaction() {
        return getTransactionBuilder().build();
    }

    static Transaction getSecondTransaction() {
        return getTransactionBuilder()
                .amount(TRANSACTION_2_AMOUNT)
                .creditAccount(ACCOUNT_NUMBER_3)
                .debitAccount(ACCOUNT_NUMBER_2)
                .build();
    }

    private static Transaction.TransactionBuilder getTransactionBuilder() {
        return Transaction.builder()
                .amount(TRANSACTION_1_AMOUNT)
                .creditAccount(ACCOUNT_NUMBER_1)
                .debitAccount(ACCOUNT_NUMBER_2);
    }

    private Fixture() {
    }
}
