package com.rudnick.competition.greenenergy.atmservice.service;

import com.rudnick.competition.greenenergy.atmservice.model.ATM;
import com.rudnick.competition.greenenergy.atmservice.model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Comparator.comparingInt;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toCollection;

@Service
public class AtmsApiService {

    @Autowired
    private final TaskToATMMapper taskToATMMapper;

    public AtmsApiService(final TaskToATMMapper taskToATMMapper) {
        this.taskToATMMapper = taskToATMMapper;
    }

    public List<ATM> calculate(final List<Task> tasks) {
        requireNonNull(tasks);

        final List<Task> sortedTasks = sortTasksByRegionAndRequestType(tasks);

        return new ArrayList<>(sortedTasks.stream()
                .map(taskToATMMapper::map)
                .collect(toCollection(LinkedHashSet::new)));
    }

    private List<Task> sortTasksByRegionAndRequestType(final List<Task> tasks) {
        return tasks.stream()
                .sorted(comparingInt(Task::getRegion)
                        .thenComparing(Task::getRequestType))
                .collect(Collectors.toList());
    }
}