package com.rudnick.competition.greenenergy.atmservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Objects;

public class Task {
    @JsonProperty("region")
    private Integer region;

    @JsonProperty("requestType")
    private RequestTypeEnum requestType;

    @JsonProperty("atmId")
    private Integer atmId;

    Task(Integer region, RequestTypeEnum requestType, Integer atmId) {
        this.region = region;
        this.requestType = requestType;
        this.atmId = atmId;
    }

    public static TaskBuilder builder() {
        return new TaskBuilder();
    }

    /**
     * Get region
     * minimum: 1
     * maximum: 9999
     *
     * @return region
     */
    @Min(1)
    @Max(9999)
    public Integer getRegion() {
        return region;
    }

    /**
     * Type of request
     *
     * @return requestType
     */
    public RequestTypeEnum getRequestType() {
        return requestType;
    }

    /**
     * Get atmId
     * minimum: 1
     * maximum: 9999
     *
     * @return atmId
     */
    @Min(1)
    @Max(9999)
    public Integer getAtmId() {
        return atmId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Task task = (Task) o;
        return Objects.equals(this.region, task.region) &&
                Objects.equals(this.requestType, task.requestType) &&
                Objects.equals(this.atmId, task.atmId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(region, requestType, atmId);
    }

    public static class TaskBuilder {
        private Integer region;
        private RequestTypeEnum requestType;
        private Integer atmId;

        TaskBuilder() {
        }

        public TaskBuilder region(Integer region) {
            this.region = region;
            return this;
        }

        public TaskBuilder requestType(RequestTypeEnum requestType) {
            this.requestType = requestType;
            return this;
        }

        public TaskBuilder atmId(Integer atmId) {
            this.atmId = atmId;
            return this;
        }

        public Task build() {
            return new Task(region, requestType, atmId);
        }

        public String toString() {
            return "Task.TaskBuilder(region=" + this.region + ", requestType=" + this.requestType + ", atmId=" + this.atmId + ")";
        }
    }
}

