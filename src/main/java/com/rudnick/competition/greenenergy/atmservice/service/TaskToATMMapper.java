package com.rudnick.competition.greenenergy.atmservice.service;

import com.rudnick.competition.greenenergy.atmservice.model.ATM;
import com.rudnick.competition.greenenergy.atmservice.model.Task;
import org.springframework.stereotype.Component;

import static java.util.Objects.requireNonNull;

@Component
public class TaskToATMMapper {
    ATM map(final Task task) {
        requireNonNull(task);

        return ATM.builder()
                .atmId(task.getAtmId())
                .region(task.getRegion())
                .build();
    }
}