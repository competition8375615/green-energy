package com.rudnick.competition.greenenergy.atmservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Objects;

public class ATM {
    @JsonProperty("region")
    private Integer region;

    @JsonProperty("atmId")
    private Integer atmId;

    private ATM(Integer region, Integer atmId) {
        this.region = region;
        this.atmId = atmId;
    }

    public static ATMBuilder builder() {
        return new ATMBuilder();
    }

    /**
     * Get region
     * minimum: 1
     * maximum: 9999
     *
     * @return region
     */
    @Min(1)
    @Max(9999)
    public Integer getRegion() {
        return region;
    }

    /**
     * Get atmId
     * minimum: 1
     * maximum: 9999
     *
     * @return atmId
     */
    @Min(1)
    @Max(9999)
    public Integer getAtmId() {
        return atmId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ATM ATM = (ATM) o;
        return Objects.equals(this.region, ATM.region) &&
                Objects.equals(this.atmId, ATM.atmId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(region, atmId);
    }

    public static class ATMBuilder {
        private Integer region;
        private Integer atmId;

        ATMBuilder() {
        }

        public ATMBuilder region(Integer region) {
            this.region = region;
            return this;
        }

        public ATMBuilder atmId(Integer atmId) {
            this.atmId = atmId;
            return this;
        }

        public ATM build() {
            return new ATM(region, atmId);
        }

        public String toString() {
            return "ATM.ATMBuilder(region=" + this.region + ", atmId=" + this.atmId + ")";
        }
    }
}

