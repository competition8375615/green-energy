package com.rudnick.competition.greenenergy.atmservice.model;

public enum RequestTypeEnum {
    FAILURE_RESTART,
    PRIORITY,
    STANDARD,
    SIGNAL_LOW;

    RequestTypeEnum() {
    }
}