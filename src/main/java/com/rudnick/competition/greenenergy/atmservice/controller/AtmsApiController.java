package com.rudnick.competition.greenenergy.atmservice.controller;

import com.rudnick.competition.greenenergy.atmservice.model.ATM;
import com.rudnick.competition.greenenergy.atmservice.model.Task;
import com.rudnick.competition.greenenergy.atmservice.service.AtmsApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/atms")
public class AtmsApiController {

    @Autowired
    private final AtmsApiService service;

    public AtmsApiController(final AtmsApiService service) {
        this.service = service;
    }

    /**
     * POST /atms/calculateOrder
     * Calculates ATMs order for service team
     *
     * @param tasks (required)
     * @return Successful operation (status code 200)
     */
    @PostMapping(
            value = "/calculateOrder",
            produces = {"application/json"},
            consumes = {"application/json"}
    )
    public ResponseEntity<List<ATM>> calculate(@Valid @RequestBody List<Task> tasks) {
        return ResponseEntity.ok(service.calculate(tasks));
    }
}
