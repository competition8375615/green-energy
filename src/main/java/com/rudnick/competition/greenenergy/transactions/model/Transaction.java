package com.rudnick.competition.greenenergy.transactions.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Objects;

public class Transaction {
    @JsonProperty("debitAccount")
    private String debitAccount;

    @JsonProperty("creditAccount")
    private String creditAccount;

    @JsonProperty("amount")
    private BigDecimal amount;

    private Transaction(String debitAccount, String creditAccount, BigDecimal amount) {
        this.debitAccount = debitAccount;
        this.creditAccount = creditAccount;
        this.amount = amount;
    }

    public static TransactionBuilder builder() {
        return new TransactionBuilder();
    }

    /**
     * Get debitAccount
     *
     * @return debitAccount
     */
    @Size(min = 26, max = 26)
    public String getDebitAccount() {
        return debitAccount;
    }

    /**
     * Get creditAccount
     *
     * @return creditAccount
     */
    @Size(min = 26, max = 26)
    public String getCreditAccount() {
        return creditAccount;
    }

    /**
     * Get amount
     *
     * @return amount
     */
    @Valid
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Transaction transaction = (Transaction) o;
        return Objects.equals(this.debitAccount, transaction.debitAccount) &&
                Objects.equals(this.creditAccount, transaction.creditAccount) &&
                Objects.equals(this.amount, transaction.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(debitAccount, creditAccount, amount);
    }

    public static class TransactionBuilder {
        private String debitAccount;
        private String creditAccount;
        private BigDecimal amount;

        TransactionBuilder() {
        }

        public TransactionBuilder debitAccount(String debitAccount) {
            this.debitAccount = debitAccount;
            return this;
        }

        public TransactionBuilder creditAccount(String creditAccount) {
            this.creditAccount = creditAccount;
            return this;
        }

        public TransactionBuilder amount(BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        public Transaction build() {
            return new Transaction(debitAccount, creditAccount, amount);
        }

        public String toString() {
            return "Transaction.TransactionBuilder(debitAccount=" + this.debitAccount + ", creditAccount=" + this.creditAccount + ", amount=" + this.amount + ")";
        }
    }
}

