package com.rudnick.competition.greenenergy.transactions.controller;

import com.rudnick.competition.greenenergy.transactions.model.Account;
import com.rudnick.competition.greenenergy.transactions.model.Transaction;
import com.rudnick.competition.greenenergy.transactions.service.TransactionsApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/transactions")
public class TransactionsApiController {

    @Autowired
    private final TransactionsApiService service;

    public TransactionsApiController(final TransactionsApiService service) {
        this.service = service;
    }

    /**
     * POST /transactions/report
     * Execute report
     *
     * @param transactions (required)
     * @return Successful operation (status code 200)
     */
    @PostMapping(
            value = "/report",
            produces = {"application/json"},
            consumes = {"application/json"}
    )
    public ResponseEntity<List<Account>> report(@Valid @RequestBody List<Transaction> transactions) {
        return ResponseEntity.ok(service.report(transactions));
    }
}