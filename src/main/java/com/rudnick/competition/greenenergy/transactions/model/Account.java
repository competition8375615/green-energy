package com.rudnick.competition.greenenergy.transactions.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Objects;

public class Account {
    @JsonProperty("account")
    private String account;

    @JsonProperty("debitCount")
    private Integer debitCount;

    @JsonProperty("creditCount")
    private Integer creditCount;

    @JsonProperty("balance")
    private BigDecimal balance;

    private Account(String account, Integer debitCount, Integer creditCount, BigDecimal balance) {
        this.account = account;
        this.debitCount = debitCount;
        this.creditCount = creditCount;
        this.balance = balance;
    }

    public static AccountBuilder builder() {
        return new AccountBuilder();
    }

    /**
     * Get account
     *
     * @return account
     */
    @Size(min = 26, max = 26)
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * Number of debit transactions
     *
     * @return debitCount
     */
    public Integer getDebitCount() {
        return debitCount;
    }

    public void setDebitCount(Integer debitCount) {
        this.debitCount = debitCount;
    }

    /**
     * Number of credit transactions
     *
     * @return creditCount
     */
    public Integer getCreditCount() {
        return creditCount;
    }

    public void setCreditCount(Integer creditCount) {
        this.creditCount = creditCount;
    }

    /**
     * Get balance
     *
     * @return balance
     */
    @Valid
    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Account account = (Account) o;
        return Objects.equals(this.account, account.account) &&
                Objects.equals(this.debitCount, account.debitCount) &&
                Objects.equals(this.creditCount, account.creditCount) &&
                Objects.equals(this.balance, account.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(account, debitCount, creditCount, balance);
    }

    public static class AccountBuilder {
        private String account;
        private Integer debitCount;
        private Integer creditCount;
        private BigDecimal balance;

        AccountBuilder() {
        }

        public AccountBuilder account(String account) {
            this.account = account;
            return this;
        }

        public AccountBuilder debitCount(Integer debitCount) {
            this.debitCount = debitCount;
            return this;
        }

        public AccountBuilder creditCount(Integer creditCount) {
            this.creditCount = creditCount;
            return this;
        }

        public AccountBuilder balance(BigDecimal balance) {
            this.balance = balance;
            return this;
        }

        public Account build() {
            return new Account(account, debitCount, creditCount, balance);
        }

        public String toString() {
            return "Account.AccountBuilder(account=" + this.account + ", debitCount=" + this.debitCount + ", creditCount=" + this.creditCount + ", balance=" + this.balance + ")";
        }
    }
}

