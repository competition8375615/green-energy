package com.rudnick.competition.greenenergy.transactions.service;

import com.rudnick.competition.greenenergy.transactions.model.Account;
import com.rudnick.competition.greenenergy.transactions.model.Transaction;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

import static java.util.Objects.requireNonNull;

@Service
public class TransactionsApiService {

    TransactionsApiService() {
    }

    public List<Account> report(final List<Transaction> transactions) {
        requireNonNull(transactions);

        final Map<String, Account> accounts = new HashMap<>();

        for (Transaction transaction : transactions) {
            processTransaction(accounts, transaction.getCreditAccount(), transaction.getAmount(), true);
            processTransaction(accounts, transaction.getDebitAccount(), transaction.getAmount().negate(), false);
        }

        final List<Account> accountList = new ArrayList<>(accounts.values());
        accountList.sort(Comparator.comparing(Account::getAccount));

        return accountList;
    }

    private void processTransaction(final Map<String, Account> accounts, final String accountNumber, final BigDecimal amount, final boolean isCredit) {
        accounts.merge(accountNumber, createNewAccount(accountNumber, amount, isCredit),
                (existingAccount, newAccount) -> {
                    if (isCredit) {
                        existingAccount.setCreditCount(existingAccount.getCreditCount() + 1);
                    } else {
                        existingAccount.setDebitCount(existingAccount.getDebitCount() + 1);
                    }
                    existingAccount.setBalance(existingAccount.getBalance().add(amount));
                    return existingAccount;
                });
    }

    private Account createNewAccount(final String accountNumber, final BigDecimal amount, final boolean isCredit) {
        final Account.AccountBuilder account = Account.builder().account(accountNumber);
        if (isCredit) {
            account.creditCount(1).debitCount(0).balance(amount);
        } else {
            account.creditCount(0).debitCount(1).balance(amount);
        }
        return account.build();
    }
}