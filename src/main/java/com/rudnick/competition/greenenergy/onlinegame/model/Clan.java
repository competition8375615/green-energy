package com.rudnick.competition.greenenergy.onlinegame.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Objects;

public class Clan {
    @JsonProperty("numberOfPlayers")
    private Integer numberOfPlayers;

    @JsonProperty("points")
    private Integer points;

    private Clan(Integer numberOfPlayers, Integer points) {
        this.numberOfPlayers = numberOfPlayers;
        this.points = points;
    }

    public static ClanBuilder builder() {
        return new ClanBuilder();
    }

    /**
     * Get numberOfPlayers
     * minimum: 1
     * maximum: 1000
     *
     * @return numberOfPlayers
     */
    @Min(1)
    @Max(1000)
    public Integer getNumberOfPlayers() {
        return numberOfPlayers;
    }

    /**
     * Get points
     * minimum: 1
     * maximum: 1000000
     *
     * @return points
     */
    @Min(1)
    @Max(1000000)
    public Integer getPoints() {
        return points;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Clan clan = (Clan) o;
        return Objects.equals(this.numberOfPlayers, clan.numberOfPlayers) &&
                Objects.equals(this.points, clan.points);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numberOfPlayers, points);
    }

    public static class ClanBuilder {
        private Integer numberOfPlayers;
        private Integer points;

        ClanBuilder() {
        }

        public ClanBuilder numberOfPlayers(Integer numberOfPlayers) {
            this.numberOfPlayers = numberOfPlayers;
            return this;
        }

        public ClanBuilder points(Integer points) {
            this.points = points;
            return this;
        }

        public Clan build() {
            return new Clan(numberOfPlayers, points);
        }

        public String toString() {
            return "Clan.ClanBuilder(numberOfPlayers=" + this.numberOfPlayers + ", points=" + this.points + ")";
        }
    }
}

