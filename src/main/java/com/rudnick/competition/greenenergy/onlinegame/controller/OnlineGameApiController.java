package com.rudnick.competition.greenenergy.onlinegame.controller;

import com.rudnick.competition.greenenergy.onlinegame.model.Clan;
import com.rudnick.competition.greenenergy.onlinegame.model.Players;
import com.rudnick.competition.greenenergy.onlinegame.service.OnlineGameApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/onlinegame")
public class OnlineGameApiController {

    @Autowired
    private final OnlineGameApiService service;

    public OnlineGameApiController(final OnlineGameApiService service) {
        this.service = service;
    }

    /**
     * POST /onlinegame/calculate
     * Calculate order
     *
     * @param players (required)
     * @return Successful operation (status code 200)
     */
    @PostMapping(
            value = "/calculate",
            produces = {"application/json"},
            consumes = {"application/json"}
    )
    public ResponseEntity<List<List<Clan>>> calculate(@Valid @RequestBody Players players) {
        return ResponseEntity.ok(service.calculate(players));
    }
}
