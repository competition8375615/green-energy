package com.rudnick.competition.greenenergy.onlinegame.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;

public class Players {
    @JsonProperty("groupCount")
    private Integer groupCount;

    @JsonProperty("clans")
    @Valid
    private List<Clan> clans;

    private Players(Integer groupCount, List<Clan> clans) {
        this.groupCount = groupCount;
        this.clans = clans;
    }

    public static PlayersBuilder builder() {
        return new PlayersBuilder();
    }

    /**
     * Number of players in single group
     * minimum: 1
     * maximum: 1000
     *
     * @return groupCount
     */
    @Min(1)
    @Max(1000)
    public Integer getGroupCount() {
        return groupCount;
    }


    /**
     * Get clans
     *
     * @return clans
     */
    @Valid
    @Size(max = 20000)
    public List<Clan> getClans() {
        return clans;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Players players = (Players) o;
        return Objects.equals(this.groupCount, players.groupCount) &&
                Objects.equals(this.clans, players.clans);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupCount, clans);
    }

    public static class PlayersBuilder {
        private Integer groupCount;
        private List<Clan> clans;

        PlayersBuilder() {
        }

        public PlayersBuilder groupCount(Integer groupCount) {
            this.groupCount = groupCount;
            return this;
        }

        public PlayersBuilder clans(List<Clan> clans) {
            this.clans = clans;
            return this;
        }

        public Players build() {
            return new Players(groupCount, clans);
        }

        public String toString() {
            return "Players.PlayersBuilder(groupCount=" + this.groupCount + ", clans=" + this.clans + ")";
        }
    }
}

