package com.rudnick.competition.greenenergy.onlinegame.service;

import com.rudnick.competition.greenenergy.onlinegame.model.Clan;
import com.rudnick.competition.greenenergy.onlinegame.model.Players;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

@Service
public class OnlineGameApiService {

    OnlineGameApiService() {
    }

    public List<List<Clan>> calculate(final Players players) {
        requireNonNull(players);

        final int maxGroupSize = players.getGroupCount();
        final List<Clan> clansSortedByPoints = getClansSortedByPoints(players);

        return calculateEntryOrder(maxGroupSize, clansSortedByPoints);
    }

    private List<List<Clan>> calculateEntryOrder(final int maxGroupSize, final List<Clan> clansSortedByPoints) {
        final List<List<Clan>> entryOrder = new ArrayList<>();

        while (!clansSortedByPoints.isEmpty()) {
            int remainingGroupSize = maxGroupSize;
            final List<Clan> actualGroup = new ArrayList<>();

            for (int clanIndex = 0; clanIndex < clansSortedByPoints.size(); clanIndex++) {
                final Clan clan = clansSortedByPoints.get(clanIndex);
                if (clan.getNumberOfPlayers() <= remainingGroupSize) {
                    actualGroup.add(clan);
                    clansSortedByPoints.remove(clanIndex--);
                    remainingGroupSize = remainingGroupSize - clan.getNumberOfPlayers();
                    if (remainingGroupSize == 0) {
                        break;
                    }
                }
            }
            entryOrder.add(actualGroup);
        }
        return entryOrder;
    }

    private List<Clan> getClansSortedByPoints(final Players players) {
        return players.getClans().stream()
                .sorted(Comparator.comparingInt(Clan::getPoints).reversed())
                .collect(Collectors.toList());
    }
}
