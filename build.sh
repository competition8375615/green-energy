# Pobranie kodu źródłowego z repozytorium
git clone https://gitlab.com/competition8375615/green-energy.git

# Przejście do katalogu z kodem źródłowym
cd green-energy

# Kompilacja kodu źródłowego
mvn compile

# Sprawdzenie, czy kompilacja się powiodła
if [ $? -ne 0 ]; then
    echo "Błąd podczas kompilacji."
    exit 1
fi

# Stworzenie pliku JAR
mvn package

# Sprawdzenie, czy stworzenie pliku JAR się powiodło
if [ $? -ne 0 ]; then
    echo "Błąd podczas tworzenia pliku JAR."
    exit 1
fi

echo "Budowanie programu zakończone sukcesem."